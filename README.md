Minimalistic C++ wrapper around V4L2
====================================

Build the demo
--------------

You need the V4L2 development package. On Debian/Ubuntu:
```Shell
$ sudo apt install libv4l2-dev
```

Then:
```Shell
mkdir build 
cd build
cmake ..&&make -j4
```

Calling `./build/webcame.out` acquires one image from `/dev/video0` and saves it as
`frame.ppm`.