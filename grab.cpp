#include <iostream>
#include <fstream>
#include "opencv2/opencv.hpp"
#include <opencv2/videoio.hpp>
#include "webcam.h"

#define XRES 1920
#define YRES 1080

using namespace std;
int num_frame = 0;
int main(int argc, char **argv)
{
    cv::namedWindow("frame", 0);
    cv::resizeWindow("frame", 500,500);
    Webcam webcam("/dev/video2", XRES, YRES);
    clock_t start = clock();
    while (true)
    {
        auto frame = webcam.frame();
        num_frame += 1;
        if (num_frame % 50 == 0)
        {
            float total_time = float(clock() - start) / CLOCKS_PER_SEC;
            cout << "FPS: " << 50 / total_time << endl;
            start = clock();
        }

        ofstream image;
        image.open("frame.ppm");
        // cv::Mat outImg(frame.width, frame.height, CV_8UC3);
        // //data copy using memcpy function
        // memcpy(outImg.data, frame.data, sizeof(unsigned char)*frame.width*frame.height*3);
        image << "P6\n"
              << XRES << " " << YRES << " 255\n";
        image.write((char *)frame.data, frame.size);
        image.close();
        cv::Mat outImg = cv::imread("frame.ppm");
        cv::imshow("frame", outImg);
        cv::waitKey(1);
    }

    return 0;
}
